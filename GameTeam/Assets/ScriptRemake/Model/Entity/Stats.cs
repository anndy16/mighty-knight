﻿using System;
using UnityEngine;

namespace ScriptRemake.Model.Entity
{
    public class Stats : MonoBehaviour
    {
        [SerializeField] private float hitPoint;
        [SerializeField] private float maxHitPoint;
        [SerializeField] private float normalSpeed;
        [SerializeField] private float maxSpeed;
        [SerializeField] private float attackDamage;
        [SerializeField] private float defensive;
        [SerializeField] private float attackSpeed;
        [SerializeField] private float antiKnockback;
        [SerializeField] private float knockback;
        [SerializeField] private float evasion;
        [SerializeField] private float accuracy;
        [SerializeField] private float currentStamina;
        [SerializeField] private float maxStamina;
        [SerializeField] private float staminaRegenSpeed = 0.1f;
        
        private void Start()
        {
            
            hitPoint = maxHitPoint;
            currentStamina = maxStamina;
        }
        public void FixedUpdate()
        {
            if (currentStamina < maxStamina)
            {
                currentStamina = currentStamina + staminaRegenSpeed > maxStamina
                    ? maxStamina
                    : currentStamina + staminaRegenSpeed;
            }
        }

        public float CurrentStamina
        {
            get => currentStamina;
            set => currentStamina = value;
        }

        public float MaxStamina
        {
            get => maxStamina;
            set => maxStamina = value;
        }

        public float StaminaRegenSpeed
        {
            get => staminaRegenSpeed;
            set => staminaRegenSpeed = value;
        }

        public float HitPoint
        {
            get => hitPoint;
            set => hitPoint = value;
        }

        public float MaxHitPoint
        {
            get => maxHitPoint;
            set => maxHitPoint = value;
        }

        public float NormalSpeed
        {
            get => normalSpeed;
            set => normalSpeed = value;
        }

        public float MaxSpeed
        {
            get => maxSpeed;
            set => maxSpeed = value;
        }

        public float AttackDamage
        {
            get => attackDamage;
            set => attackDamage = value;
        }

        public float Defensive
        {
            get => defensive;
            set => defensive = value;
        }

        public float AttackSpeed
        {
            get => attackSpeed;
            set => attackSpeed = value;
        }

        public float AntiKnockback
        {
            get => antiKnockback;
            set => antiKnockback = value;
        }

        public float Knockback
        {
            get => knockback;
            set => knockback = value;
        }

        public float Evasion
        {
            get => evasion;
            set => evasion = value;
        }

        public float Accuracy
        {
            get => accuracy;
            set => accuracy = value;
        }
    }
}