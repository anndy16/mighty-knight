using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LoseSceneButtonController : MonoBehaviour
{
    public Button playAgain;
    public Button backToMain;
    public Button Quit;
    void Start()
    {
        playAgain.onClick.AddListener(PlayAgains);
        backToMain.onClick.AddListener(BackToMain);
        Quit.onClick.AddListener(Quits);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void BackToMain(){
        SceneManager.LoadScene("StartMenuScene");
    }
    void PlayAgains(){
        SceneManager.LoadScene("PlayScene");
    }
    void Quits(){
        Application.Quit();
    }   
}
