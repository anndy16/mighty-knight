using System.Collections;
using System.Collections.Generic;
using ScriptRemake.Model.Entity;
using UnityEngine;

[CreateAssetMenu]
public class CharacterStatMovementSpeedModifierSO : CharacterStatModifierSO
{
    public override void AffectCharacter(GameObject character, float val)
    {
        Stats stats = character.GetComponent<Stats>();
        if (stats != null)
        {
            stats.NormalSpeed += val;
            stats.MaxSpeed += val;
        }
    }
}
