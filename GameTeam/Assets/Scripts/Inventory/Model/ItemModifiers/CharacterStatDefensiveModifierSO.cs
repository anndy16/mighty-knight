using System.Collections;
using System.Collections.Generic;
using ScriptRemake.Model.Entity;
using UnityEngine;

[CreateAssetMenu]
public class CharacterStatDefensiveModifierSO : CharacterStatModifierSO
{
    public override void AffectCharacter(GameObject character, float val)
    {
        Stats stats = character.GetComponent<Stats>();
        if (stats != null && stats.Defensive != null)
        {
            stats.Defensive += val;
        }
    }
}
