using System.Collections;
using System.Collections.Generic;
using ScriptRemake.Model.Entity;
using UnityEngine;

[CreateAssetMenu]
public class CharacterStatHealthModifierSO : CharacterStatModifierSO
{
    public override void AffectCharacter(GameObject character, float val)
    {
        Stats stats = character.GetComponent<Stats>();
        if (stats != null && stats.HitPoint != null)
        {
            float value = stats.HitPoint + val;
            stats.HitPoint = (value >= stats.MaxHitPoint ? stats.MaxHitPoint : value);
        }
    }
}
