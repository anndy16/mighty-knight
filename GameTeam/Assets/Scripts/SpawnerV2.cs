using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SpawnerV2 : MonoBehaviour
{
    public GameObject spawnee;
    public bool stopSpawning = false;
    public float spawnTime = 10;
    public float spawnDelay = 2;
    public int limitedEntity = 15;
    private int entitySpawned = 0;
    public Vector2 randomPos = Vector2.zero;
    // Use this for initialization
    void Start () {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    private void Update()
    {
        if (entitySpawned >= limitedEntity)
        {
            stopSpawning = true;
        }
    }

    public void SpawnObject() {
        Instantiate(spawnee, transform.position + new Vector3(Random.Range(-randomPos.x, randomPos.x),Random.Range(-randomPos.y, randomPos.y),0.0f), transform.rotation);
        entitySpawned++;
        if(stopSpawning) {
            CancelInvoke("SpawnObject");
        }
    }
}
