using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusBar : MonoBehaviour
{
    public Slider slider;
    private float maxValue;
    private float current;

    public void SetMaxValue(float currentValue, float maxValue)
    {
        this.maxValue = maxValue;
        slider.value = (currentValue) / maxValue;
    }

    public void SetHealth(float value)
    {
        slider.value = value / maxValue;
    }

    public void SetMaxHealth(float maxValue)
    {
        this.maxValue = maxValue;
    }
}
