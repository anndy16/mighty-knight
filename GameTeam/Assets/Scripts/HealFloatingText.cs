using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealFloatingText : MonoBehaviour
{
    public float timeLife = 0.5f;
    public float floatSpeed = 250;
    private RectTransform rectTransform;
    private float timeElapsed = 0f;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        timeElapsed += Time.deltaTime;
        rectTransform.position += Vector3.up * floatSpeed * Time.deltaTime;
        if (timeElapsed > timeLife)
        {
            Destroy(gameObject);
        }
    }
}
