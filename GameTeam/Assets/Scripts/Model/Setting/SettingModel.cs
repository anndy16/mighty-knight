using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingModel : MonoBehaviour
{


    public static SettingModel SettingInstance{get; set;}
    private void Awake() {
         if(SettingInstance != null){
            Destroy(gameObject);
        }else{
            SettingInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        
    }

    
    private float _masterVolume;
    public float MasterVolume{
        get{
            return _masterVolume;
        }
        set{
            _masterVolume = value;
        }
    }
    private bool _isFullScreen;
    public bool IsFullScreen{
        get{
            return _isFullScreen;
        }
        set{
            _isFullScreen = value;
        }
    }
    public Resolution resolution;
    public Resolution Resolution{
        get{
            return resolution;
        }
        set{
            resolution = value;
        }
    }

    public int dropDownIndex;
    public int DropDownIndex{
        get{
            return dropDownIndex;
        }
        set{
            dropDownIndex = value;
        }
    }

    private bool _ingame;
    public bool Ingame{
        get{
            return _ingame;
        }
        set{
            _ingame = value;
        }
    }
   
    
    
}
