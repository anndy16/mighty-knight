using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeRecordDisplay : MonoBehaviour
{
    public Text timeDisplay;
    // Start is called before the first frame update
    void Start()
    {
        int minutes = Mathf.FloorToInt(GameManage.timeRecord / 60f);
        int seconds = Mathf.FloorToInt(GameManage.timeRecord - minutes*60);
        timeDisplay.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
