using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class SettingController : MonoBehaviour
{
    
    public Slider slider;
     public Toggle fullScreenToggle;

    [SerializeField] TMP_Dropdown resolutionDropdown;
    
    Resolution[] resolutions;
    void Start()
    {
       slider.value = SettingModel.SettingInstance.MasterVolume;
       SoundSourceController.SoundInstance.audioMixer.SetFloat("masterVolume", SettingModel.SettingInstance.MasterVolume);
       slider.onValueChanged.AddListener(setVolume);
        Screen.SetResolution(SettingModel.SettingInstance.resolution.width, SettingModel.SettingInstance.resolution.height, Screen.fullScreen);
       fullScreenToggle.isOn = SettingModel.SettingInstance.IsFullScreen;
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        for(int i = 0; i < resolutions.Length; i++){
            string option = resolutions[i].ToString();
            
            options.Add(option);
        }
        
        
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = SettingModel.SettingInstance.DropDownIndex;
        resolutionDropdown.onValueChanged.AddListener(SetSolution);

        fullScreenToggle.onValueChanged.AddListener(setScreenMode);
    }

    void setVolume(float volume){
        SoundSourceController.SoundInstance.audioMixer.SetFloat("masterVolume", volume);
        SettingModel.SettingInstance.MasterVolume = volume;
    }
    // Start is called before the first frame update
    
     void SetSolution(int index){
        Screen.SetResolution(resolutions[index].width, resolutions[index].height, Screen.fullScreen);
        SettingModel.SettingInstance.Resolution = resolutions[index];
         SettingModel.SettingInstance.DropDownIndex = index;
        
    }

    public void setScreenMode(bool isFullScreen){
        Screen.fullScreen = isFullScreen;
        SettingModel.SettingInstance.IsFullScreen = isFullScreen;
    }

    // Update is called once per frame
    
}
