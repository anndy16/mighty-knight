using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SettingWindowController : MonoBehaviour
{
   
    public Toggle fullScreenToggle;

    [SerializeField] TMP_Dropdown resolutionDropdown;
    
    Resolution[] resolutions;

    
    
    // Start is called before the first frame update
    void Start()
    {
        
        
        fullScreenToggle.isOn = SettingModel.SettingInstance.IsFullScreen;
        resolutions = Screen.resolutions;

        resolutionDropdown.ClearOptions();

        List<string> options = new List<string>();

        for(int i = 0; i < resolutions.Length; i++){
            string option = resolutions[i].ToString();
            
            options.Add(option);
        }
        
        
        resolutionDropdown.AddOptions(options);
        resolutionDropdown.onValueChanged.AddListener(SetSolution);
    }

    void SetSolution(int index){
        Screen.SetResolution(resolutions[index].width, resolutions[index].height, Screen.fullScreen);
        SettingModel.SettingInstance.Resolution = resolutions[index];
        
    }

    // Update is called once per frame
    void Update()
    {
    
       
    }

    public void setFullScreen(bool isFullScreen){
        Screen.fullScreen = isFullScreen;
        SettingModel.SettingInstance.IsFullScreen = isFullScreen;
    }
}
