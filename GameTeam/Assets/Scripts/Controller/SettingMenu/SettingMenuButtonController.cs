using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingMenuButtonController : MonoBehaviour
{
    
    public Button btnBack;
    // Start is called before the first frame update
    void Start()
    {   

        btnBack.onClick.AddListener(BackToMain);
    }

    void BackToMain(){
        SceneManager.LoadScene("StartMenuScene");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
