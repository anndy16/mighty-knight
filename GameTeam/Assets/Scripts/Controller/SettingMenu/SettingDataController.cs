using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingDataController : MonoBehaviour
{
    
    public static SettingDataController Instance{get; private set;}

    

    void Awake() {
        if(Instance != null){
            Destroy(this.gameObject);

        }else{
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Hello");
        LoadSettings();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnApplicationQuit() {
        SaveSettings();
        Debug.Log("hehe");
        
    }
    
    void LoadSettings(){
        if(!PlayerPrefs.HasKey("firstTimeOpenGame")){
            PlayerPrefs.SetInt("firstTimeOpenGame", 0);
            
            SettingModel.SettingInstance.IsFullScreen = true;
            SettingModel.SettingInstance.MasterVolume = 5f;
            SettingModel.SettingInstance.resolution.height = 1920;
            SettingModel.SettingInstance.resolution.width = 1080;
            Debug.Log("First time open game");
        }else{
            
            SettingModel.SettingInstance.IsFullScreen = Convert.ToBoolean(PlayerPrefs.GetInt("isFullScreen"));
            GameManage.timeRecord = PlayerPrefs.GetFloat("timeRecord");
            SettingModel.SettingInstance.MasterVolume = PlayerPrefs.GetFloat("masterVolume");
            SettingModel.SettingInstance.resolution.width = PlayerPrefs.GetInt("resolutionWidth");
            SettingModel.SettingInstance.resolution.height = PlayerPrefs.GetInt("resolutionHeight");
            SettingModel.SettingInstance.DropDownIndex = PlayerPrefs.GetInt("dropDownIndex");
            Debug.Log("Not first time open game");
        }
        
    }

    void SaveSettings(){
        PlayerPrefs.SetFloat("timeRecord", GameManage.timeRecord);
        PlayerPrefs.SetFloat("masterVolume", SettingModel.SettingInstance.MasterVolume);
        PlayerPrefs.SetInt("isFullScreen", Convert.ToInt32(SettingModel.SettingInstance.IsFullScreen));
        PlayerPrefs.SetInt("resolutionWidth", (SettingModel.SettingInstance.resolution.width));
        PlayerPrefs.SetInt("resolutionHeight", (SettingModel.SettingInstance.resolution.height));
        PlayerPrefs.SetInt("dropDownIndex", SettingModel.SettingInstance.DropDownIndex);
        Debug.Log("Saved");
    }
}
