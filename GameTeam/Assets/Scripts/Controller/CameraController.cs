using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform trackObject;
    
    void Update()
    {
        // transform.position = Vector2.Lerp(transform.position, trackObject.position, Time.deltaTime * 2.0f);
        transform.position = new Vector3(trackObject.position.x, trackObject.position.y);
    }
}
