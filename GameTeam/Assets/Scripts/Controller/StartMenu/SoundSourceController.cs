using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundSourceController : MonoBehaviour
{
    public static SoundSourceController SoundInstance {get; private set;}
    // Start is called before the first frame update
    public AudioMixer audioMixer;
    void Awake() {
        AudioListener.pause = false;
        if(SoundInstance != null){
            Destroy(gameObject);
        }else{
            SoundInstance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    void Start()
    {
        
        SoundInstance.audioMixer.SetFloat("masterVolume", SettingModel.SettingInstance.MasterVolume);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
