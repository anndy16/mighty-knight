using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenuButtonController : MonoBehaviour
{

    Button btnStart;
    Button btnContinue;
    Button btnCredit;
    Button btnQuit;

    Button btnHowToPlay;
    Button btnSetting;

    void OnEnable(){
          Screen.SetResolution(SettingModel.SettingInstance.resolution.width, SettingModel.SettingInstance.resolution.height, Screen.fullScreen);
    }

    

    // Start is called before the first frame update
    void Start()
    {
        
      
        btnStart = GameObject.Find("View/StartButton").GetComponent<Button>();
        btnContinue = GameObject.Find("View/ContinueButton").GetComponent<Button>();
        btnSetting = GameObject.Find("View/SettingButton").GetComponent<Button>();
        btnCredit = GameObject.Find("View/CreditButton").GetComponent<Button>();
        btnQuit = GameObject.Find("View/QuitButton").GetComponent<Button>();
        btnHowToPlay = GameObject.Find("View/HowToPlayButton").GetComponent<Button>();
       
        btnStart.onClick.AddListener(startNewGame);
        btnContinue.onClick.AddListener(continueFromLastSave);
        btnSetting.onClick.AddListener(openSettingConsole);
        btnCredit.onClick.AddListener(openCredits);
        btnQuit.onClick.AddListener(exitGame);
        btnHowToPlay.onClick.AddListener(openHowToPlay);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void startNewGame(){

        SceneManager.LoadScene("PlayScene");
        
    }

    void continueFromLastSave(){
         SceneManager.LoadScene("TimeRecordScene");
    }
    void openCredits(){
        SceneManager.LoadScene("CreditScene");
    }

    void openHowToPlay(){
        SceneManager.LoadScene("HTPScene");
    }
    void openSettingConsole(){
        SceneManager.LoadScene("SettingMenuScene");
    }

    void exitGame(){
        Application.Quit();
    }

}
