﻿using System;
using System.Collections;
using System.Collections.Generic;
using ScriptRemake.Model.Entity;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DefaultNamespace
{
    public class SlimeController : MonoBehaviour
    {
        private Stats stats;
        private Animator _animator;
        private Rigidbody2D _rigidbody2D;
        private bool isDie = false;
        private SpriteRenderer _spriteRenderer;
        private bool isAttacked = false;
        private Collider2D _collider2D;
        private MobDetectZone mobDetectZone;
        [SerializeField] private Vector2 direction;
        [SerializeField] private bool canMove = true;
        [SerializeField] private bool isRoaming = false;

        [Header("Random Roaming")]
        [SerializeField] private float range;
        [SerializeField] private float distance;
        [SerializeField] private Vector2 wayPoint;

        [Header("attack")] [SerializeField] private float radius;
        [SerializeField] private LayerMask enemyLayerMask;
        [SerializeField] private float randomDistanceStartAtk = 0.5f;
        [SerializeField] private float distanceStartAtk = 1.0f;
        private float timeBtwAtk;
        private float startTimeBtwAtk;
        private void Start()
        {
            _animator = GetComponent<Animator>();
            _rigidbody2D = GetComponent<Rigidbody2D>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            stats = GetComponent<Stats>();
            _collider2D = GetComponent<Collider2D>();
            mobDetectZone = GetComponentInChildren<MobDetectZone>();
            startTimeBtwAtk = 1.0f / stats.AttackSpeed + 0.5f;
        }

        private void FixedUpdate()
        {
            if (!isDie)
            {
                CheckDie();
            }
            StopTarget();
            if (mobDetectZone.detectObjs.Count > 0)
            {
                Vector3 dir = (mobDetectZone.detectObjs[0].transform.position - transform.position).normalized;
                direction = new Vector2(dir.x, dir.y);
                isRoaming = false;
            }
            Move();
            RandomAtk();
        }
        
        private void CheckDie()
        {
            if (stats.HitPoint <= 0f)
            {
                canMove = false;
                _animator.SetTrigger("isDie");
                Destroy(gameObject, 1.5f);
                isDie = true;
                _collider2D.enabled = false;
            }
        }

        public void TakeDamage(float damage, float accuracy, float knockBack, Vector3 attackerPosition)
        {
            _animator.SetTrigger("attacked");
            float dmgDeal = 0.0f;
            isAttacked = true;
            if (accuracy >= stats.Evasion * 1.5f)
            {
                dmgDeal = damage * 1.5f - stats.Defensive;
            } else if (accuracy >= stats.Evasion)
            {
                dmgDeal = damage * 1.1f - stats.Defensive;
            } else if (accuracy >= stats.Evasion / 2)
            {
                dmgDeal = damage * 0.7f - stats.Defensive;
            }
            else
            {
                dmgDeal = damage * 0.5f - stats.Defensive;
            }
            stats.HitPoint -= dmgDeal > 0.0f ? dmgDeal : 0.0f;
            _rigidbody2D.AddForce((transform.position - attackerPosition).normalized * 
                                  (knockBack - stats.AntiKnockback));
        }

        private void Move()
        {
            if (canMove)
            {
                if (isRoaming == false)
                {
                    _rigidbody2D.AddForce(direction * 3000 * stats.NormalSpeed * Time.fixedDeltaTime);
                }
                else
                {
                    Vector2 moveTo = Vector2.MoveTowards(transform.position, wayPoint,
                    stats.NormalSpeed / 5.0f * Time.deltaTime);
                    _rigidbody2D.MovePosition(moveTo);
                    if (Vector2.Distance(transform.position, wayPoint) < range)
                    {
                        SetNewDestination();
                    }
                }
                _animator.SetBool("isMoving", true);
            }
            else
            {
                _animator.SetBool("isMoving", false);
            }
        }

        private void Update()
        {
            if (direction.x < 0)
            {
                _spriteRenderer.flipX = true;
            }
            else if (direction.x > 0)
            {
                _spriteRenderer.flipX = false;
            }

            if (direction == Vector2.zero)
            {
                _animator.SetBool("isMoving", false);
            }
        }

        void StopTarget()
        {
            if (mobDetectZone.detectObjs.Count == 0)
            {
                direction = Vector2.zero;
            }
            isRoaming = true;
        }

        private void SetNewDestination()
        {
            wayPoint = new Vector2(Random.Range(-distance, distance), Random.Range(-distance, distance));
            _animator.SetBool("isMoving", true);
        }

        private void Attack()
        {
            if (timeBtwAtk <= 0)
            {
                _animator.SetTrigger("isAttack");
                Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(transform.position, radius, 
                    enemyLayerMask);
                foreach (Collider2D enemy in hitEnemies)
                {
                    Debug.Log("hit enemy" + enemy.name);
                    TestMove en = enemy.GetComponent<TestMove>();
                    en.TakeDamage(stats.AttackDamage, stats.Accuracy, stats.Knockback, transform.position);
                }
                timeBtwAtk = startTimeBtwAtk;
            }
            else
            {
                timeBtwAtk -= Time.fixedDeltaTime;
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.DrawWireSphere(transform.position, radius);
        }
        
        public void UnlockMove()
        {
            canMove = true;
        }

        public void LockMove()
        {
            canMove = false;
        }

        private void RandomAtk()
        {
            if (!isRoaming)
            {
                float dis = Vector2.Distance(transform.position, mobDetectZone.detectObjs[0].transform.position);
                if (dis + Random.Range(-randomDistanceStartAtk, randomDistanceStartAtk) <= 1.0f)
                {
                    Attack();
                }
            }
        }
    }
}