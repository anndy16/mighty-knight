using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SkillsCooldownDisplay : MonoBehaviour
{
    private String skill1CD;
    private String skill2CD;
    [SerializeField] private TextMeshProUGUI skill1Display;
    [SerializeField] private TextMeshProUGUI skill2Display;
    [SerializeField] private TestMove playerControll;

    private void Update()
    {
        skill1CD = playerControll.Skill1Time.ToString("f1");
        skill2CD = playerControll.Skill2Time.ToString("f1");
        if (playerControll.Skill1Time <= 0)
        {
            skill1CD = "Ready";
        }
        if (playerControll.Skill2Time <= 0)
        {
            skill2CD = "Ready";
        }
        skill1Display.text = "<color=\"red\">Skill AOE: <color=\"white\"> " + skill1CD;
        skill2Display.text = "<color=\"green\">Skill Heal: <color=\"white\"> " + skill2CD;
    }
}
