using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using ScriptRemake.Model.Entity;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestMove : MonoBehaviour
{
    private AudioSource Audio;
    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    public ContactFilter2D movementFilter;
    private List<RaycastHit2D> castCollisions = new List<RaycastHit2D>();
    public Vector2 lastDirection;
    [SerializeField] private float speed;
    private Vector2 direction;
    private float timeBtwAttack;
    private float startTimeBtwAttack;
    private bool canMove = true;
    private Stats stats;
    [SerializeField] private float atkRange;
    [SerializeField] private Transform atkUpPoint;
    [SerializeField] private Transform atkDownPoint;
    [SerializeField] private Transform atkLeftPoint;
    [SerializeField] private Transform atkRightPoint;
    [SerializeField] private LayerMask enemyLayer;
    private Vector2 pos;
    private bool isSprint = false;
    [SerializeField] private StatusBar staminaBar;
    [SerializeField] private StatusBar healBar;
    [SerializeField] private float currentSpeed;
    
    [Header("Skill 1")]
    [SerializeField] private float skill1CD = 5.0f;
    [SerializeField] private float skill1Time;
    [SerializeField] private bool useSkill1 = false;
    [SerializeField] private GameObject skill1Entity;
    [Header("Skill 2")]
    [SerializeField] private float skill2CD = 30.0f;
    [SerializeField] private float skill2Time;
    [SerializeField] private bool useSkill2 = false;
    [SerializeField] private GameObject skill2Entity;
    
    private void Start()
    {
        Audio =  GetComponent<AudioSource>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        stats = GetComponent<Stats>();
        healBar.SetMaxValue(stats.HitPoint, stats.MaxHitPoint);
        staminaBar.SetMaxValue(stats.CurrentStamina, stats.MaxStamina);
        currentSpeed = stats.NormalSpeed;
        startTimeBtwAttack = 1.0f / stats.AttackSpeed + 0.5f;
    }

    private void Update()
    {
        InputHandle();

        _animator.SetFloat("moveX", direction.x);
        _animator.SetFloat("moveY", direction.y);
        
        if (direction.x == 1 ||
            direction.y == 1 ||
            direction.x == -1 ||
            direction.y == -1)
        {
            _animator.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
            _animator.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
            lastDirection.x = direction.x;
            lastDirection.y = direction.y;
        }

        if (stats.CurrentStamina <= stats.MaxStamina * 10 / 100)
        {
            isSprint = false;
        }
        
    }

    private void FixedUpdate()
    {
        StartCoroutine("CheckDie");
        Direction();
        SetSpeed();
        Move();
        Attack();
        Skills();
        healBar.SetHealth(stats.HitPoint);
        staminaBar.SetHealth(stats.CurrentStamina);
        
    }

    private void Attack()
    {
        if (timeBtwAttack <= 0)
        {
            if (Input.GetKey(KeyCode.J))
            {
                Audio.Play();
                _animator.SetTrigger("isAttack");
                Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(pos, atkRange, enemyLayer);
                foreach (Collider2D enemy in hitEnemies)
                {
                    SlimeController en = enemy.GetComponent<SlimeController>();
                    en.TakeDamage(stats.AttackDamage, stats.Accuracy, stats.Knockback, transform.position);
                }
                timeBtwAttack = startTimeBtwAttack;
            }
        }
        else
        {
            timeBtwAttack -= Time.fixedDeltaTime;
        }
    }

    private void Move()
    {
        direction.Normalize();
        if (canMove)
        {
            if (direction != Vector2.zero)
            {
                bool success = TryMove(direction);
                if (!success)
                {
                    success = TryMove(new Vector2(direction.x, 0));
                    if (!success)
                    {
                        success = TryMove(new Vector2(0, direction.y));
                    }
                }
            }

            if (direction.x < 0)
            {
                _spriteRenderer.flipX = true;
            }
            else if (direction.x > 0)
            {
                _spriteRenderer.flipX = false;
            }
        }
    }
    
    private bool TryMove(Vector2 direction)
    {
        if (direction == Vector2.zero)
            return false;
        int count = _rigidbody2D.Cast(direction, 
            movementFilter, 
            castCollisions, 
            currentSpeed * Time.fixedDeltaTime);
        if (count == 0)
        {
            _rigidbody2D.MovePosition(_rigidbody2D.position + direction * currentSpeed * Time.fixedDeltaTime);
            return true;
        }
        return false;
    }

    public void UnlockMove()
    {
        canMove = true;
    }

    public void LockMove()
    {
        canMove = false;
    }

    public IEnumerator CheckDie()
    {
        if (stats.HitPoint <= 0)
        {
            canMove = false;
            _animator.SetTrigger("isDie");
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("LoseScene");
        }
    }

    public void TakeDamage(float damage, float accuracy, float knockBack, Vector3 attackerPosition)
    {
        float dmgDeal = 0.0f;
        if (accuracy >= stats.Evasion * 1.5f)
        {
            dmgDeal = damage * 1.5f - stats.Defensive;
        } else if (accuracy >= stats.Evasion)
        {
            dmgDeal = damage * 1.1f - stats.Defensive;
        } else if (accuracy >= stats.Evasion / 2)
        {
            dmgDeal = damage * 0.7f - stats.Defensive;
        }
        else
        {
            dmgDeal = damage * 0.5f - stats.Defensive;
        }
        stats.HitPoint -= dmgDeal > 0.0f ? dmgDeal : 0.0f;
        _rigidbody2D.AddForce((transform.position - attackerPosition).normalized * 
                              (knockBack - stats.AntiKnockback));
    }
    
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(atkUpPoint.position, atkRange);
        Gizmos.DrawWireSphere(atkDownPoint.position, atkRange);
        Gizmos.DrawWireSphere(atkLeftPoint.position, atkRange);
        Gizmos.DrawWireSphere(atkRightPoint.position, atkRange);
        Gizmos.DrawWireSphere(pos, 5);
    }

    private void Direction()
    {
        if (direction.x == 1)
        {
            pos = atkRightPoint.position;
        } else if (direction.x == -1)
        {
            pos = atkLeftPoint.position;
        } else if (direction.y == 1)
        {
            pos = atkUpPoint.position;
        } else if (direction.y == -1)
        {
            pos = atkDownPoint.position;
        }
    }

    void SetSpeed()
    {
        if (isSprint && stats.CurrentStamina > 0)
        {
            currentSpeed = stats.MaxSpeed;
            stats.CurrentStamina -= 1f;
        }
        else
        {
            currentSpeed = stats.NormalSpeed;
            stats.CurrentStamina = stats.CurrentStamina + stats.StaminaRegenSpeed > stats.MaxStamina
                ? stats.MaxStamina
                : stats.CurrentStamina + stats.StaminaRegenSpeed;
        }
    }
    
    void InputHandle()
    {
        direction.x = Input.GetAxisRaw("Horizontal");
        direction.y = Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.LeftShift))
        {
            isSprint = true;
        }
        else
        {
            isSprint = false;
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            if (skill1Time <= 0)
                useSkill1 = true;
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            if (skill2Time <= 0)
                useSkill2 = true;
        }
    }


void Skills()
    {
        SkillsFirst();
        SkillsSecond();
    }

    void SkillsFirst()
    {
        if (skill1Time <= 0)
        {
            if (useSkill1)
            {
                _animator.SetTrigger("isAttack");
                Instantiate(skill1Entity, pos, transform.rotation);
                Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(pos, 5, enemyLayer);
                foreach (Collider2D enemy in hitEnemies)
                {
                    SlimeController en = enemy.GetComponent<SlimeController>();
                    en.TakeDamage(stats.AttackDamage * 10.0f, stats.Accuracy * 2.0f, stats.Knockback, transform.position);
                }
                skill1Time = skill1CD;
                useSkill1 = false;
            }
        }
        else
        {
            skill1Time -= Time.fixedDeltaTime;
        }
    }

    void SkillsSecond()
    {
        if (skill2Time <= 0)
        {
            if (useSkill2)
            {
                _animator.SetTrigger("isAttack");
                Instantiate(skill2Entity, transform.position, transform.rotation);
                float value = stats.HitPoint + stats.MaxHitPoint * 10.0f / 100.0f;
                stats.HitPoint = (value >= stats.MaxHitPoint ? stats.MaxHitPoint : value);
                skill2Time = skill2CD;
                useSkill2 = false;
            }
        }
        else
        {
            skill2Time -= Time.fixedDeltaTime;
        }
    }

    public float Skill1Time
    {
        get => skill1Time;
        set => skill1Time = value;
    }

    public float Skill2Time
    {
        get => skill2Time;
        set => skill2Time = value;
    }
}
