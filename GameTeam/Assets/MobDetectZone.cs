using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MobDetectZone : MonoBehaviour
{
    public List<Collider2D> detectObjs;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            detectObjs.Add(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player" && detectObjs.Count > 0)
        {
            detectObjs.Remove(other);
        }
    }
}
