using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillController : MonoBehaviour
{
    [SerializeField] private float timeLife = 3.0f;
    void Start()
    {
        Destroy(gameObject, timeLife);
    }
}
