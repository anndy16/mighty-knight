using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManage : MonoBehaviour
{
    
    private bool isPause = false;
    private bool bossSpawned = false;

    public static float time;
    public static float timeRecord;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject settingMenu;
    [SerializeField] private GameObject boss;
    [SerializeField] private Vector2 playerPosition;
    private void Start()
    {
        
        time = 0.0f;

        settingMenu.SetActive(false);
        pausePanel.SetActive(false);
        playerPosition = GameObject.FindWithTag("Player").transform.position;
    }

    private void Update()
    {
        
        time += Time.deltaTime;
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Time.timeScale = 1 - Time.timeScale;
            AudioListener.pause = Time.timeScale > 0 ? false : true;
            pausePanel.SetActive(AudioListener.pause);
        }

        if(time>timeRecord){
            timeRecord = time;
        }

        if (!bossSpawned)
        {
            StartCoroutine("spawnBoss");
            bossSpawned = true;
        }
    }

    public void Resume()
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        pausePanel.SetActive(false);
    }

    public void BackToMain()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("StartMenuScene");
    }

    public void Setting()
    {
        settingMenu.SetActive(true);
    }

    public void Back()
    {
        settingMenu.SetActive(false);
    }

    public void HowToPlayScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("HTPScene");
    }

    public void ExitProgram()
    {
        Application.Quit();
    }

    private IEnumerator spawnBoss()
    {
        yield return new WaitForSeconds(25);
        Instantiate(boss, playerPosition + new Vector2(15f, 15f), transform.rotation);
    }
}
