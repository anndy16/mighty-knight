using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeDisplay : MonoBehaviour
{
    public Text timeDisplay ;
    
    // Start is called before the first frame update
    void Start()
    {
        
        timeDisplay.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        int minutes = Mathf.FloorToInt(GameManage.time / 60f);
        int seconds = Mathf.FloorToInt(GameManage.time - minutes*60);
        timeDisplay.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
